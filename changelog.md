# Dereth Forever - GameWorld Change Log - February 2018

### 2018-02-20

**Behemoth**

* Initial export of json database cutover.

### 2018-02-14

**Behemoth**

* DF_world 1.0.0 release for opening source, includes db rename
* Pruned older releases

### 2018-02-08

**Gold Golem**

* Updated release to DF8, created by Coral Golem.
* Added Hex output for Bulk-Export.

### 2018-02-02

**Coral Golem**

* Updated release to DF7.
* Rebased for changes to database.
* Ran bulk json import to update all matching weenies.   I have omitted addind the did for clothing base to allow our workaround to continue to work until we are actually spawning clothing items on NPC's.
